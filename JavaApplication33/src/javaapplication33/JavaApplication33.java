/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication33;

import java.util.Random;

/**
 *
 * @author kj
 */
public class JavaApplication33 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        final int N = 10;
        int[] wektor = new int[N];
        
        for(int i = 0; i < N; i++)
            tab[ i ] = new Random().nextInt(); 
        
        int min = tab[0];
        int max = tab[0];
        for(int i = 0; i < N; i++){
            if( min > tab[i] ) min = tab[i];
            if( max < tab[i] ) max = tab[i];
        }
        
        System.out.println( "min="+min + " max=" + max );
        
        boolean zamiana = true;
        for(int i = 0; i < N && zamiana; i++){
            zamiana = false;
            for(int j = 0; j < N-1; j++){
                if( tab[j]>tab[j+1] ){
                    int pom = tab[j];
                    tab[j]=tab[j+1];
                    tab[j+1]=pom;
                    zamiana = true;
                }
            }
        }        
    }
    
}
